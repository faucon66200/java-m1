package com.film;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Movie {
    private String title;
    private String genre;
    private String length;
    private String synopsis;

    public Movie() {
    }

    public Movie(String title, String genre, String length, String synopsis) {
        this.title = title;
        this.genre = genre;
        this.length = length;
        this.synopsis = synopsis;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", length='" + length + '\'' +
                ", synopsis='" + synopsis + '\'' +
                '}';
    }

    public JSONObject toJson() {
        JSONObject film = new JSONObject();
        film.put("titre",this.title);
        film.put("genre",this.genre);
        film.put("durée",this.length);
        film.put("synopsis",this.synopsis);
        return film;
    }
}
