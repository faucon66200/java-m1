import com.film.Movie;
import com.itextpdf.commons.exceptions.ITextException;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Document;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;
import java.util.List;

import static com.itextpdf.kernel.geom.PageSize.A4;

public class Main {
    private String pathCsv = "/home/m1bdai/java/filsJson/data/data.csv";

    private String pathJson = "/home/m1bdai/java/filsJson/data/data.json";

    private JTextField title = new JTextField();
    private JTextField duree = new JTextField();
    private JTextField synopsis = new JTextField();
    private JTextField genre = new JTextField();
    private JLabel label = new JLabel("Un JTextField");
    static JDialog modelDialog = new JDialog();
    //ne sert que si le fichier json est vide
    public void initialization() {

        try {
            //lecture du stream CSV
            FileInputStream inputStream = new FileInputStream(pathCsv);
            Scanner scanner = new Scanner(inputStream);//parcours du csv

            //List<Movie> movies = new ArrayList<Movie>();


            int count = 0;
            while (scanner.hasNextLine()) {
                //System.out.println(count);
                String[] movie = scanner.nextLine().split(";");
                if (count != 0) {
                    /*System.out.println(movie.length);*/
                    Movie film = new Movie(cleanString(movie[0]), cleanString(movie[1]), cleanString(movie[2]), cleanString(movie[3]));//creation d'un objet Movie
                    this.writeJason(film);
                    //movies.add(film);
                }
                count++;
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private String cleanString(String str){
        return str.replaceAll("\"","");
    }
    //lecture de jason

    public JSONArray readJason(){
        JSONParser jasonParser = new JSONParser();
        JSONArray jasonArray = new JSONArray();
        try
        {
            FileReader fr = new FileReader(pathJson);//lecture
            jasonArray = (JSONArray) jasonParser.parse(fr);

        }
        catch (IOException e)
        {
            throw new RuntimeException(e.getMessage());
        }
        catch (ParseException e)
        {
            System.out.println(e.getMessage());
            return jasonArray;
        }
        return jasonArray;
    }
    //get the data
    public Object[][] showDataJson(){
        List<Object[]> data = new ArrayList<Object[]>();//je crée une liste de tableau d'objet
        JSONArray json = this.readJason();
        json.forEach(film -> {// parcours jason
            JSONObject f = (JSONObject) film;//récupére objet jason
            Object[] ff = {f.get("titre"),f.get("genre"),f.get("durée"),f.get("synopsis")};//je créé mon tableau d'objet
            data.add(ff);//je l'ajoute a ma liste
        });
        Object[][] skull = new Object[data.size()][]; // je créé un tableau d'objet multidimensionnel
        // dont la premiore dimension est la taille de ma liste
        for(int i=0; i < skull.length; i++){//je parscours la liste
            skull[i] = data.get(i);// jassigne
        }
        return skull;
    }
    //ecrire l'objet
    public void writeJason(@NotNull Movie movie){
        try {
            JSONArray movieJson = this.readJason();
            movieJson.add(movie.toJson());

            FileWriter fw = new FileWriter(this.pathJson);
            fw.write(movieJson.toJSONString());
            fw.flush();

        }catch (IOException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    //init de la fenetre graphique
    public void windows(){
        JFrame frame = new JFrame("Mes films");

        JPanel panelMenu = new JPanel();
        panelMenu.setBackground(Color.red);
        panelMenu.setLayout(new BoxLayout(panelMenu,BoxLayout.X_AXIS));
        panelMenu.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));


        JLabel menuTitle = new JLabel();
        menuTitle.setText("MENU :");
        menuTitle.setForeground(Color.GREEN);
        menuTitle.setToolTipText("Ceci est le menu");


        JButton btn = new JButton("Ajouter un film");
        btn.addActionListener(new ActionBtn());
        final JDialog modelDialog = createDialog(frame);
        panelMenu.add(menuTitle);
        panelMenu.add(btn);

        JScrollPane tablePanel = new JScrollPane();
        String[] columnName ={"Titre","Genre","Durée","Synopsis"};
        Object[][] data = this.showDataJson();

        JTable table = new JTable(data,columnName);
        tablePanel.add(table);

        Container tableContainer = new Container();
        tableContainer.setLayout(new BorderLayout());
        tableContainer.add(table.getTableHeader(),BorderLayout.PAGE_START);
        tableContainer.add(table,BorderLayout.CENTER);


        table.setFillsViewportHeight(true);

        frame.getContentPane().add(BorderLayout.NORTH,panelMenu);
        frame.getContentPane().add(BorderLayout.CENTER,tableContainer);


        //comportement à ka fermeture de la fenetre
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       /* try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        }catch (ClassNotFoundException e){
            throw new RuntimeException(e);
        }catch (InstantiationException e){
            throw new RuntimeException(e);
        }catch (IllegalAccessException e){
            throw new RuntimeException(e);

        }catch (UnsupportedLookAndFeelException e){
            throw new RuntimeException(e);
        }*/
        frame.setSize(600,600);
        //Affiche la fenetre
        frame.setVisible(true);
    }
    public static void main(String[] args) {

        Main ma = new Main();
        JSONArray ja = ma.readJason();

        if(ja.isEmpty()){//si json est vide
            ma.initialization();
        }
        ma.windows();
        /*else{
            ma.readJason();
            *//*Movie newMovie = new Movie("Le retour du roi","Heroic fantasy","3h40","C'est le meilleur des seigneurs");
            ma.writeJason(newMovie);*//*
        }*/

    }

    private @NotNull JDialog createDialog(final JFrame frame)
    {
        modelDialog = new JDialog(frame, "Ajouter un film",Dialog.ModalityType.DOCUMENT_MODAL);
        //taille et positionnement de la modale
        modelDialog.setBounds(250,250,300,400);

        JButton print = new JButton("Print");

        print.addActionListener(new ActionPrint());

        JTextField title = new JTextField("titre du film");
        JLabel titleForm = new JLabel("Titre du film");
        title.setPreferredSize(new Dimension(150, 30));

        JTextField genre = new JTextField("genre du film");
        JLabel genreForm = new JLabel("genre du film");

        JTextField duree = new JTextField("durée du film");
        JLabel dureeForm = new JLabel("durée du film");

        JTextField synopsis = new JTextField("Synopsis du film");
        JLabel sysnopsisForm = new JLabel("Sysnopsis du film");



        /*Container dialogContainer = modelDialog.getContentPane();
        dialogContainer.setLayout(new BorderLayout());
        dialogContainer.add(new JLabel("cest de la merde"),BorderLayout.CENTER);
*/
        Container formContainer = modelDialog.getContentPane();
        formContainer.setLayout(new BorderLayout());
        //création d'un container qui est en mode box layout pour les champs du formulaire
        Box box = new Box(BoxLayout.Y_AXIS);

        box.add(titleForm);
        box.add(title);

        box.add(genreForm);
        box.add(genre);

        box.add(dureeForm);
        box.add(duree);

        box.add(sysnopsisForm);
        box.add(synopsis);

        formContainer.add(box);
        JPanel pan = new JPanel();


        //pan.add(titleForm,BorderLayout.NORTH);
        //pan.add(title);
        //pan.add(genre);
        pan.setLayout(new FlowLayout());
        JButton close = new JButton("ok");
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("TEXT : genre " + genre.getText());
                System.out.println("TEXT : duree " + duree.getText());
                System.out.println("TEXT : title " + title.getText());
                System.out.println("TEXT : synopsis " + synopsis.getText());
                Movie newMovie = new Movie(title.getText(),genre.getText(),duree.getText(),synopsis.getText());
                writeJason(newMovie);
                modelDialog.setVisible(false);
            }
        });
        pan.add(close);
        formContainer.add(pan,BorderLayout.SOUTH);

        return modelDialog;
    }
private class ActionBtn implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e) {
        modelDialog.setVisible(true);
    }

}

    private class ActionPrint implements ActionListener {
        private String pdfPath = "/home/m1bdai/java/filsJson/data/data.csv";
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                PdfWriter writer = new PdfWriter(this.pdfPath);

                PdfDocument pdfDocument = new PdfDocument(writer);
                Document document = new Document(pdfDocument, A4);
                document.setMargins(2.5f,2.5f,2.5f,2.5f);
                PdfPage page = pdfDocument.addNewPage();
                PdfCanvas pdfCanvas = new PdfCanvas(page);
                Canvas canvas = new Canvas(pdfCanvas,pdfDocument,table.print());

            }catch (ITextException | FileNotFoundException ex){
                throw new RuntimeException(ex.getMessage());
            }
        }
    }
}