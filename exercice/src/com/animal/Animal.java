package com.animal;

public abstract class Animal {
    Shout shout;

    public void action() {
        shout.shout();
    }
}

