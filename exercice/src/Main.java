import com.animal.Animal;
import com.animal.Chat;
import com.animal.Chien;
import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Random;

public class Main {
    static Random random = new Random();
    static @NotNull Animal generateAnimal(){
        return (Math.abs(random.nextInt()) % 2 == 0) ? new Chien() : new Chat();
    }
    public static void main(String[] args) {

        //Random random = new Random();


        /*Animal chien = new Chien();

        chien.action();
        Animal chat = new Chat();

        chat.action();*/
        generateAnimal().action();

        //System.out.println("Hello world!");
    }


}