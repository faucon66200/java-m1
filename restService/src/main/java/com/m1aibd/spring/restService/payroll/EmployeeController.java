package com.m1aibd.spring.restService.payroll;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController{

    private final EmployeeRepository repository;

    public EmployeeController(EmployeeRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/employees")
    List<Employee> all(){
        return repository.findAll();
    }
    @PostMapping("/employees")
    Employee newEmployee(@RequestBody Employee newEmployee){
        return repository.save(newEmployee);
    }
    @GetMapping("/employees/{id}")
    Employee one(@PathVariable Long id){
        return repository.findById(id).orElseThrow(() -> new EmployeeNotFoundException(id));
    }
    @PutMapping("/employee/{id}")
    Employee replaceEmployee(@PathVariable Long id, @RequestBody Employee replaceEmployee){
        return repository.findById(id).map(emp -> {
            emp.setName(replaceEmployee.getName());
            emp.setRole(replaceEmployee.getRole());
            return repository.save(emp);
        }).orElseGet(()->{
            replaceEmployee.setId(id);
            return repository.save(replaceEmployee);
        });
    }
    @DeleteMapping("/employees/{id}")
    void deleteEmployee(@PathVariable Long id){
        repository.deleteById(id);
    }
}
