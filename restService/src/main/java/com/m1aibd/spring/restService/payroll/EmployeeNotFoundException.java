package com.m1aibd.spring.restService.payroll;

public class EmployeeNotFoundException extends RuntimeException{
    public EmployeeNotFoundException(Long id){
        super("Could not find the employee number : " + id);
    }
}
