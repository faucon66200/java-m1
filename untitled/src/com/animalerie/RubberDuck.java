package com.animalerie;

public class RubberDuck extends Duck{
    public RubberDuck() {
        flyBehavior = new FlyNoWay();
    }

    /**
     *
     */
    @Override
    void swim() {

    }
}
