package com.animalerie;

public class Chien extends Animal implements Communication{
    private String race;

    private int age;
    private String couleur;
    private String name;
    private boolean lof;

    /*public void cri(){
        System.out.println("haaaaaaaaaaan");
    }*/
    public String attaque(boolean reussi){
        return (reussi) ? ("bien croqué le poulet") : ("il a pris un coup de schlass par kader");
    }

    public boolean isLof() {
        return lof;
    }

    public void setLof(boolean lof) {
        this.lof = lof;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getCouleur() {
        return couleur;
    }

    public String getName() {
        return name;
    }

    public Chien() {
        super("mammal");

    }
    public static int nbr_pattes = 7;

    public Chien(String race, int age, String couleur, String name, boolean lof, String type) {
        super("Mammal");
        //this.type = type;
        this.race = race;
        this.age = age;
        this.couleur = couleur;
        this.name = name;
        this.lof = lof;
    }

    @Override
    public String toString() {
        return "Chien{" +
                "race='" + race + '\'' +
                ", age=" + age +
                ", couleur='" + couleur + '\'' +
                ", name='" + name + '\'' +
                ", lof=" + lof +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public void crier() {
        System.out.println("haaaaaaaaaaan je viens de l'interface pd");
    }
}
