package com.animalerie;

public class Animal {
    protected String type;

    public String getType() {
        return type;
    }

    public Animal(String type) {
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
