package com.animalerie;

public class FlyNoWay implements FlyBehavior{
    /**
     *
     */
    @Override
    public void fly() {
        System.out.println("Non je peut pas voler");
    }
}
