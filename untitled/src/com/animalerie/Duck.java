package com.animalerie;

public abstract class Duck {

    FlyBehavior flyBehavior;

    abstract void swim();

    public void display(){
        System.out.println("Show me your duck ;) ");
    }
    public void performQuack(){
        System.out.println("Coin coin");
    }
    public void performFly(){
        flyBehavior.fly();
    }

}
